Write-Host "Build clickonce application and create Octopus release candidate"
invoke-expression -Command .\build.ps1
$buildDir = resolve-path .\
cd $buildDir\..\src\WpfApplication1\bin\Release\
Write-Host "Simulate Octopus Deploy and invoke deploy.ps1"
invoke-expression -Command .\deployment\deploy.ps1
cd $buildDir